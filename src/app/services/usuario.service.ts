import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { UsuarioModel } from '../models/usuario.models';

@Injectable({
  providedIn: 'root'
})
export class UsuarioService {

  constructor(private http: HttpClient) { }


  regUsuario(user: UsuarioModel){

    return this.http.post("http://localhost/usuarioapp/public/api/regUsuario", user);
  }
}
