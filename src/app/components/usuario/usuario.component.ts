import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { UsuarioModel } from 'src/app/models/usuario.models';
import { UsuarioService } from 'src/app/services/usuario.service';

@Component({
  selector: 'app-usuario',
  templateUrl: './usuario.component.html',
  styleUrls: ['./usuario.component.css']
})
export class UsuarioComponent implements OnInit {

  user= new UsuarioModel;
  constructor( private usuarioService:UsuarioService) { }

  ngOnInit(): void {
  }

  guardar(form:NgForm){

    if(form.invalid){
      console.log("Formulario Incompleto");
      return;
      
    }
    
    this.usuarioService.regUsuario(this.user).subscribe(resp=>{
        alert("Usuario registrado correctamente");
        window.location.href='/home';
    },
    error=>{
      alert("Ha ocurrido un error");
    });
    
    
  }

}
